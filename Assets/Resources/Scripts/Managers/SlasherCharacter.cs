using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlasherCharacter : MonoBehaviour
{
    [Header("Movement")]
    private CharacterController2D _controller;
    private float _movement = 0f;
    [SerializeField] private float _runSpeed = 40f;
    private bool _isJumping;
    private bool _isRunning;
    private bool _isCrouching;
    private bool _isDashing;

    [Header("Visuals")]
    [SerializeField] private Animator _animator;


    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController2D>();
        // _controller.SetAbilities();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _animator.SetBool("isMoving", _movement != 0f);
    }

    public void SetMovement(float movement)
    {
        _movement = movement * _runSpeed;
    }

    public void SetJumping(bool isJumping)
    {
        _isJumping = isJumping;
    }

    public void SetCrouching(bool isCrouching)
    {
        _isCrouching = isCrouching;
        // set visuals
        if (_isCrouching)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    public void SetRunning(bool isRunning)
    {
        _isRunning = isRunning;
        // set visuals
    }

    public void SetDashing(bool isDashing)
    {
        _isDashing = isDashing;
    }

    public void Move(float time)
    {
        _controller.Move(_movement * time, _isCrouching, _isJumping, _isRunning, _isDashing);
        _isJumping = false;
        _isDashing = false;
    }

    public void Attack(AttackType type)
    {

    }
}
