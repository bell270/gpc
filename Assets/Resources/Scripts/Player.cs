using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private SlasherCharacter body;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        body.SetMovement(Input.GetAxis("Horizontal"));

        if (Input.GetButtonDown("Jump"))
        {
            body.SetJumping(true);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            body.SetRunning(true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            body.SetRunning(false);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            body.SetCrouching(true);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            body.SetCrouching(false);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            body.SetDashing(true);
        }
    }

    private void FixedUpdate()
    {
        body.Move(Time.fixedDeltaTime);
    }
}
